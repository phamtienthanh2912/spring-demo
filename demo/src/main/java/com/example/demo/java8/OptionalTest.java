package com.example.demo.java8;

import com.example.demo.dto.Test;

import java.util.Optional;

public class OptionalTest {
    public static void main(String[] args) {
        Test test = Test.builder().build();
        test.setA("bb");
        Optional<Test> optional = Optional.ofNullable(test);
        if(optional.isPresent()){
            System.out.println(test.getA());
        }
        optional.ifPresent(item -> System.out.println(item.getA()));
        System.out.println(optional.map(item -> item.getA() + 1).orElse("aa"));
    }
}

