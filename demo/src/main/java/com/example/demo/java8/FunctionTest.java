package com.example.demo.java8;

@FunctionalInterface
public interface FunctionTest<U, T> {

    T test(U input);

    default String testDefaultfc() {
        return "Test default";
    }
}
