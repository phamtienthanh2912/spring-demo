package com.example.demo.java8;

public class FunctionMain {
    public String testFuncion(String param, FunctionTest<String, String> functionTest){
        return functionTest.test(param);
    }

    public Integer testFuncionInteger(String param, FunctionTest< String ,Integer> functionTest){
        return functionTest.test(param);
    }

    public static void main(String[] args) {
        FunctionMain fm = new FunctionMain();
        System.out.println(fm.testFuncion("thanh" , input -> input+"A"));
        System.out.println(fm.testFuncionInteger("1" , input -> Integer.parseInt(input)));
    }
}
