package com.example.demo.controller;

import com.example.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("api1")
public class Test2Controller implements Serializable {
    @Autowired
    CustomerService customerService;

    @GetMapping("/abc1")
    public void intData() {
        try {
            customerService.setTest("Thanhpr");
            Thread.sleep(8000);
            System.out.println(customerService + "|" + customerService.getTest());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}