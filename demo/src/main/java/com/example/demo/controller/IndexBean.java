package com.example.demo.controller;

import com.example.demo.service.CustomerService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class IndexBean implements Serializable {
    private String test;

    @ManagedProperty("#{khachHangServiceImpl)")
    CustomerService khachHangServiceImpl;

    @PostConstruct
    public void intData(){
        System.out.println("-----------------------PostConstruct-----------------");
        test = "Hello";
    }

    public IndexBean() {
        System.out.println("---------------------------contruct-------------------------");
        test = "Hello from PrimeFaces and Spring Boot!";
    }

    public void hello(){
        System.out.println("Hello");
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}