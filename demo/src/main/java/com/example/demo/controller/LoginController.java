package com.example.demo.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@ManagedBean
@Scope("request")
public class LoginController {
    public void login(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request =  (HttpServletRequest) externalContext.getRequest();
        try {
            //redirect url
            String redirect = "";
            request.setAttribute("", "");

            externalContext.redirect(redirect);

            //login qua Spring security
            RequestDispatcher dispatcher = request.getRequestDispatcher("");
            dispatcher.forward(request, (ServletResponse) externalContext.getResponse());
            SecurityContextHolder.getContext().getAuthentication();
            //ket thuc goi
            facesContext.responseComplete();

            //
            //request.getSession().getAttribute("Test").



        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }

    }
}
