package com.example.demo.controller;

import com.example.demo.service.CustomerService;
import com.example.demo.service.TestNameable;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;

@RestController
@RequestMapping("api")
public class Test1Controller implements Serializable {
    @Autowired
    CustomerService customerService;

    @GetMapping("/abc")
    public void intData() {
        try {
            customerService.setTest("ThanhPham");
            Thread.sleep(3000);
            System.out.println(customerService + "|" + customerService.getTest());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}