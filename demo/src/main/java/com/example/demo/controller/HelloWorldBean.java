package com.example.demo.controller;

import com.example.demo.service.CustomerService;
import com.example.demo.service.TestNameable;
import com.example.mysql.model.Order;
import org.jasypt.encryption.StringEncryptor;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@Scope(value = "view")
@Component
public class HelloWorldBean  implements Serializable {
    private String test;

    @Autowired
    CustomerService customerService;

    @Autowired
    TestNameable testNameable;

    @Autowired
    Environment environment;

    private UploadedFile file;

    @Value("${encrypted.property}")
    private String encrypted;

    @Autowired
    @Qualifier("encryptorBean")
    StringEncryptor encryptorBean;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    private Double value;



    private Order order = new Order();
    private List<Order> lstorder = new ArrayList<>();

    @PostConstruct
    public void intData(){
        value = 5.26d;
        System.out.println("-----------------------PostConstruct-----------------");
        test = "Hello";
        testNameable.initData();
        System.out.println(environment.getProperty("encrypted.property") + encrypted);
        System.out.println(encryptorBean.encrypt("root"));
        System.out.println(encryptorBean.encrypt("123456789"));

        //Test test = Test.builder().a("").build();
        Order tmp = new Order();
        tmp.setId(123l);
        lstorder.add(tmp);
    }

    public HelloWorldBean() {
        System.out.println("---------------------------contruct-------------------------");
        test = "Hello from PrimeFaces and Spring Boot!";
    }

    public void hello(){
        System.out.println("Hello");
        System.out.println(file);
        testNameable.setName(String.valueOf(System.currentTimeMillis()));
        testNameable.setShow(!testNameable.isShow());
    }

    public void handleFileUpload(FileUploadEvent event){
        System.out.println(event);
    }

    public List<Order> orderFind (String key){
        return lstorder;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public TestNameable getTestNameable() {
        return testNameable;
    }

    public void setTestNameable(TestNameable testNameable) {
        this.testNameable = testNameable;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Order> getLstorder() {
        return lstorder;
    }

    public void setLstorder(List<Order> lstorder) {
        this.lstorder = lstorder;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
}