package com.example.demo.controller;

import com.example.mysql.model.Order;
import com.example.mysql.service.OrderService;
import com.example.mysql.service.StoreService;
import com.example.oracle.model.Customer;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.Future;

@RestController
public class SampleController {

    @Autowired
    StoreService storeService;

    @Autowired
    OrderService orderService;

    @ResponseBody
    @RequestMapping(value = "/allOrders")
    List<Order> home() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Order[]> response = restTemplate.getForEntity("http://localhost:8081/order/all", Order[].class);
        Order[] orderArr= response.getBody();

//        ArrayList<Future<ResponseEntity<Order[]>>> futures = new ArrayList<>();
//        AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate(customClientHttpRequestFactory);
//        futures.add(asyncRestTemplate.getForEntity(url, String.class)); //start up to 10 requests in parallel, depending on your pool

        return Arrays.asList(orderArr);



    }

    //Test truong hop rollback Exception
    @ResponseBody
    @RequestMapping(value = "/testRollback")
    String testRollback() throws Exception {
        storeService.testRollback();
        return "Test rollback";
    }

    @ResponseBody
    @RequestMapping(value = "/testRollback2")
    String testRollback2() throws Exception {
        storeService.testRollback2();
        return "Test rollback";
    }

    @ResponseBody
    @RequestMapping(value = "/store")
    Object store() {
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            Customer c = new Customer();
            c.setName("test");
            c.setAge(30);
            Order o = new Order();
            o.setCode(1);
            o.setQuantity(7);
            storeService.store(c, o);

            Assert.notNull(c.getId());
            Assert.notNull(o.getId());
            result.put("status", "0");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("status", "1");
            result.put("msg", e.getMessage());
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/storeRolback")
    Object storeRolback() throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        Customer c = new Customer();
        c.setName("test");
        c.setAge(30);
        Order o = new Order();
        o.setCode(1);
        o.setQuantity(7);
        storeService.storeWithStoreException(c, o);

        Assert.notNull(c.getId());
        Assert.notNull(o.getId());
        result.put("status", "0");
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/storeNoRolback")
    Object storeNoRolback() throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        Customer c = new Customer();
        c.setName("test");
        c.setAge(30);
        Order o = new Order();
        o.setCode(1);
        o.setQuantity(7);
        storeService.storeWithNoRollbackException(c, o);

        Assert.notNull(c.getId());
        Assert.notNull(o.getId());
        result.put("status", "0");
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/save")
    Object save() {
        Map<String, Object> result = new HashMap<String, Object>();
        try {

            storeService.transfer();
//    		storeService.transferWithStoreException();
            result.put("status", "0");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("status", "1");
            result.put("msg", e.getMessage());
        }
        return result;
    }
}
