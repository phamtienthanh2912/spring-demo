package com.example.demo.service;

public interface TestNameable {
    void initData();

    boolean isShow();

    void setShow(boolean show);


    String getName();

    void setName(String name);
}
