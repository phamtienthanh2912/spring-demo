package com.example.demo.configuration;

import com.sun.faces.config.ConfigureListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

@Configuration
public class JSFConfig {
    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        FacesServlet servlet = new FacesServlet();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.jsf");
        return servletRegistrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean<ConfigureListener> servletListenerRegistrationBean() {
        ConfigureListener configureListener = new ConfigureListener();
        ServletListenerRegistrationBean<ConfigureListener> servletListenerRegistrationBean = new ServletListenerRegistrationBean<>(
                configureListener);
        return servletListenerRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean FileUploadFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new org.primefaces.webapp.filter.FileUploadFilter());
        registration.setName("PrimeFaces FileUpload Filter");
        registration.addUrlPatterns("/*");
        registration.setDispatcherTypes(javax.servlet.DispatcherType.FORWARD, javax.servlet.DispatcherType.REQUEST);
        registration.addServletRegistrationBeans(servletRegistrationBean());
        registration.setOrder(2);
        return registration;
    }

    @Bean
    public FilterRegistrationBean hiddenHttpMethodFilterDisabled(
            @Qualifier("hiddenHttpMethodFilter") HiddenHttpMethodFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    @Bean
    public ServletContextInitializer initializer() {
        return new ServletContextInitializer() {

            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
                servletContext.setInitParameter("com.sun.faces.numberOfViewsInSession", "5");
                servletContext.setInitParameter("com.sun.faces.serializeServerState", "false");
                servletContext.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
                servletContext.setInitParameter("javax.faces.DATETIMECONVERTER_DEFAULT_TIMEZONE_IS_SYSTEM_TIMEZONE",
                        "true");
                servletContext.setInitParameter("javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL", "true");
                servletContext.setInitParameter("primefaces.FONT_AWESOME", "true");
                servletContext.setInitParameter("primefaces.THEME", "admin");
                servletContext.setInitParameter("javax.faces.VALIDATE_EMPTY_FIELDS", "true");
                servletContext.setInitParameter("faces.FACELETS_BUFFER_SIZE", "65535");
                servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
                servletContext.setInitParameter("primefaces.UPLOADER", "commons");
            }
        };
    }

}
