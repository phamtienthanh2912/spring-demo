package com.example.demo.configuration.db;

import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.mysql.cj.jdbc.MysqlXADataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaDialect;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import javax.sql.DataSource;
import java.util.HashMap;
import org.eclipse.persistence.platform.database.MySQLPlatform;
@Configuration
@DependsOn("transactionManager")
@EnableJpaRepositories(basePackages = "com.example.mysql.repository", entityManagerFactoryRef = "mysqlEntityManager",
        transactionManagerRef = "transactionManager")
public class MysqlDatabase {

    @Autowired
    Environment env;

    @Bean("mysqlJpaVendorAdapter")
    public JpaVendorAdapter mysqlJpaVendorAdapter() {
//        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
//        hibernateJpaVendorAdapter.setShowSql(true);
//        hibernateJpaVendorAdapter.setGenerateDdl(true);
//        hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
        EclipseLinkJpaVendorAdapter eclipseLinkJpaVendorAdapter = new EclipseLinkJpaVendorAdapter();
        eclipseLinkJpaVendorAdapter.setShowSql(true);
        eclipseLinkJpaVendorAdapter.setGenerateDdl(false);
        eclipseLinkJpaVendorAdapter.setDatabase(Database.MYSQL);
//        eclipseLinkJpaVendorAdapter.setDatabasePlatform("org.eclipse.persistence.platform.database.OraclePlatform");
        eclipseLinkJpaVendorAdapter.setDatabasePlatform("org.eclipse.persistence.platform.database.MySQLPlatform");
        return eclipseLinkJpaVendorAdapter;
    }

    @Bean(name = "mysqlDataSource")
    public DataSource mysqlDataSource() throws Throwable {
        MysqlXADataSource mysqlXaDataSource = new MysqlXADataSource();
        mysqlXaDataSource.setUrl(env.getProperty("mysql.spring.datasource.url"));
        mysqlXaDataSource.setPassword(env.getProperty("mysql.spring.datasource.password"));
        mysqlXaDataSource.setUser(env.getProperty("mysql.spring.datasource.username"));
        mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);
        AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
        xaDataSource.setXaDataSource(mysqlXaDataSource);
        xaDataSource.setUniqueResourceName("mysql");
        xaDataSource.setMaxPoolSize(300);
        xaDataSource.setMinPoolSize(0);
        xaDataSource.setMaxLifetime(30);
        return xaDataSource;
    }

    @Bean(name = "mysqlEntityManager")
    public LocalContainerEntityManagerFactoryBean oracleEntityManager() throws Throwable{
        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
        HashMap<String, Object> properties = new HashMap<String, Object>();
        //properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
//        properties.put("eclipselink.target-server","com.atomikos.eclipselink.platform.AtomikosPlatform");
//        properties.put("eclipselink.target-database","org.eclipse.persistence.platform.database.OraclePlatform");
        properties.put("eclipselink.target-server","com.atomikos.eclipselink.platform.AtomikosPlatform");
        properties.put("eclipselink.target-database", "org.eclipse.persistence.platform.database.MySQLPlatform");
        properties.put("javax.persistence.transactionType", "JTA");
        properties.put("eclipselink.weaving", "false");
        //properties.put("eclipselink.logging.level", "OFF");
        //properties.put("eclipselink.logging.level.sql", "OFF");
        properties.put("javax.persistence.lock.timeout", "0");
        properties.put("javax.persistence.query.timeout", "15");
        entityManager.setJtaDataSource(mysqlDataSource());
        entityManager.setJpaVendorAdapter(mysqlJpaVendorAdapter());
        entityManager.setPackagesToScan("com.example.mysql.model");
        entityManager.setPersistenceUnitName("mysqlPersistenceUnit");
        entityManager.setJpaPropertyMap(properties);
        entityManager.setJpaDialect(new EclipseLinkJpaDialect());
        return entityManager;
    }

}

