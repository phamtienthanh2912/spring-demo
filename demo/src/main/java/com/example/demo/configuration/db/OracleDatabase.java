package com.example.demo.configuration.db;

import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.mysql.cj.jdbc.MysqlXADataSource;
import oracle.jdbc.xa.OracleXADataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Properties;

@Configuration
@DependsOn("transactionManager")
@EnableJpaRepositories(basePackages = "com.example.oracle.repository", entityManagerFactoryRef = "oracleEntityManager",
        transactionManagerRef = "transactionManager")
public class OracleDatabase {

    @Autowired
    Environment env;

    @Primary
    @Bean("oracleJpaVendorAdapter")
    public JpaVendorAdapter oraclejpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
        return hibernateJpaVendorAdapter;
    }

    @Primary
    @Bean(name = "oracleDataSource")
    public DataSource oracleDataSource() throws Throwable {
//        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
//        ds.setUniqueResourceName("oracle");
//        ds.setXaDataSourceClassName("oracle.jdbc.xa.client.OracleXADataSource");
//        Properties p = new Properties();
//        p.setProperty("user", env.getProperty("boc.jdbc.username"));
//        p.setProperty("password", env.getProperty("boc.jdbc.password"));
//        p.setProperty("URL", env.getProperty("boc.jdbc.url"));
//        ds.setXaProperties(p);
//        ds.setMinPoolSize(0);
//        ds.setMaxLifetime(30);
//        ds.setMaxPoolSize(300);

//        OracleXADataSource oracleXADataSource = new oracle.jdbc.xa.client.OracleXADataSource();
//        oracleXADataSource.setURL(env.getProperty("boc.jdbc.url"));
//        oracleXADataSource.setUser(env.getProperty("boc.jdbc.username"));
//        oracleXADataSource.setPassword(env.getProperty("boc.jdbc.password"));
//        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
//        ds.setUniqueResourceName("oracle");
//        ds.setXaDataSourceClassName("oracle.jdbc.xa.client.OracleXADataSource");
//        Properties p = new Properties();
//        p.setProperty("user", env.getProperty("oracle.spring.datasource.username"));
//        p.setProperty("password", env.getProperty("oracle.spring.datasource.password"));
//        p.setProperty("URL", env.getProperty("oracle.spring.datasource.url"));
//        ds.setXaProperties(p);
//        ds.setUniqueResourceName("oracle");
//        ds.setMaxPoolSize(300);
//        ds.setMinPoolSize(0);
//        ds.setMaxLifetime(30);
//        return ds;

        MysqlXADataSource mysqlXaDataSource = new MysqlXADataSource();
        mysqlXaDataSource.setUrl(env.getProperty("mysql.spring.datasource.url.one"));
        mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);
        mysqlXaDataSource.setPassword(env.getProperty("mysql.spring.datasource.password"));
        mysqlXaDataSource.setUser(env.getProperty("mysql.spring.datasource.username"));
        mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);

        AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
        xaDataSource.setXaDataSource(mysqlXaDataSource);
        xaDataSource.setUniqueResourceName("oracle");
        xaDataSource.setMaxPoolSize(300);
        xaDataSource.setMinPoolSize(0);
        xaDataSource.setMaxLifetime(30);
        return xaDataSource;
    }

    @Primary
    @Bean(name = "oracleEntityManager")
    @DependsOn("transactionManager")
    public LocalContainerEntityManagerFactoryBean oracleEntityManager() throws Throwable{
        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
        HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
        properties.put("javax.persistence.transactionType", "JTA");
        entityManager.setJtaDataSource(oracleDataSource());
        entityManager.setJpaVendorAdapter(oraclejpaVendorAdapter());
        entityManager.setPackagesToScan("com.example.oracle.model");
        entityManager.setPersistenceUnitName("oraclePersistenceUnit");
        entityManager.setJpaPropertyMap(properties);
        return entityManager;
    }
}
