package com.example.demo.converter;


import com.example.common.inter.KeyCommon;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.ArrayList;
import java.util.List;

@FacesConverter("orderConverter")
public class OrderConverter<T extends KeyCommon> implements Converter {
    List<T> lstData = new ArrayList<>();
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
            if(lstData == null || lstData.isEmpty()) return null;
            T t = lstData.stream().filter(item -> item.getCommon().toString().equals(s)).findFirst().orElse(null);
            return t;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o == null || ((T)o).getCommon() == null) return "";
        if(o instanceof KeyCommon){
            return ((T) o).getCommon().toString();
        }
        return o.toString();
    }
    public List<T> getLstData() {
        return lstData;
    }

    public void setLstData(List<T> lstData) {
        this.lstData = lstData;
    }
}
