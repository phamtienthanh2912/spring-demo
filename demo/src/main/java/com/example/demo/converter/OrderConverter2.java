package com.example.demo.converter;


import com.example.common.inter.KeyCommon;
import com.example.mysql.model.Order;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

@FacesConverter("orderConverter2")
public class OrderConverter2 implements Converter {
    List<Order> lstData = new ArrayList<>();
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
            if(lstData == null || lstData.isEmpty()) return new Order();
            Order t = lstData.stream().filter(item -> item.getCommon().equals(s)).findFirst().orElse(new Order());
            return t;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o == null) return "";
        if(o instanceof Order){
            return (String) ((Order) o).getCommon();
        }
        return o.toString();
    }

    public List<Order> getLstData() {
        return lstData;
    }

    public void setLstData(List<Order> lstData) {
        this.lstData = lstData;
    }
}
