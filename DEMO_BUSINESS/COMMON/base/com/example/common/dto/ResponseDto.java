package com.example.common.dto;

import java.util.List;

public class ResponseDto<T> {
    private boolean success;
    private String message;
    private String desc;
    List<T> lstData;

    public ResponseDto(){

    }

    public ResponseDto(boolean success, String message, String desc) {
        this.success = success;
        this.message = message;
        this.desc = desc;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<T> getLstData() {
        return lstData;
    }

    public void setLstData(List<T> lstData) {
        this.lstData = lstData;
    }
}
