package com.example.mysql.repository;

import com.example.mysql.model.Order;

import java.util.List;

public interface OrderRepositoryCustom {
    List<Order> getListOrders();

    Order create(Order order);
}
