package com.example.mysql.repository;


import com.example.mysql.model.RedPacketAccount;
import org.springframework.data.repository.CrudRepository;

public interface RedPacketAccountRepository extends CrudRepository<RedPacketAccount, Long> {


}
