package com.example.mysql.repository;

import com.example.mysql.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> , OrderRepositoryCustom{

}
