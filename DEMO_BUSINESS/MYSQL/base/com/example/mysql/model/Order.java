package com.example.mysql.model;

import com.example.common.inter.KeyCommon;

import javax.persistence.*;

@Entity
@Table(name = "orders")
//@Data
//@EqualsAndHashCode(exclude = { "id" })
public class Order implements KeyCommon {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "code", nullable = false)
	private Integer code;

	@Column(name = "quantity", nullable = false)
	private Integer quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public Object getCommon() {
		return id;
	}
}
