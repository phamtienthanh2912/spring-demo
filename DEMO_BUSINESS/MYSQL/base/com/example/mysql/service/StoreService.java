package com.example.mysql.service;


import com.example.common.exception.NoRollbackException;
import com.example.common.exception.StoreException;
import com.example.mysql.model.Order;
import com.example.oracle.model.Customer;

public interface StoreService {
	
	void store(Customer customer, Order order) throws Exception;
	
	void storeWithStoreException(Customer customer, Order order) throws StoreException;
	
	void storeWithNoRollbackException(Customer customer, Order order) throws NoRollbackException;
	
	void transferWithStoreException() throws StoreException;
	void transferWithNoRollbackException() throws NoRollbackException;
	void transfer();

	void testRollback() throws Exception;
	void testRollback2() throws Exception;

}
