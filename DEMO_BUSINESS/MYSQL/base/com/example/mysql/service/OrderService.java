package com.example.mysql.service;

import com.example.mysql.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> getAllOrders();
    Order saveOrder(Order order) throws Exception;

    Order create(Order order) throws Exception;
}

