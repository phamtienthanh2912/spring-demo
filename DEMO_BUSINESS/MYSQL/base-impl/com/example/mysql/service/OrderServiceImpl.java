package com.example.mysql.service;

import com.example.common.exception.NoRollbackException;
import com.example.mysql.model.Order;
import com.example.mysql.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    @Cacheable(cacheNames="orderCache")
    public List<Order> getAllOrders() {
        return orderRepository.getListOrders();
    }

    //propagation = Propagation.REQUIRES_NEW
    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = NoRollbackException.class)
    public Order saveOrder(Order order) throws Exception {
        order = orderRepository.save(order);
        if (order != null) {
            throw new Exception("CHeck loi rollback");
        }
        return order;
    }

    @Override
    public Order create(Order order) throws Exception {
        return orderRepository.create(order);
    }


}
