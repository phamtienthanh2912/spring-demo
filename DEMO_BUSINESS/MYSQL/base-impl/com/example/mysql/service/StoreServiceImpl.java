package com.example.mysql.service;

import com.example.common.exception.NoRollbackException;
import com.example.common.exception.StoreException;
import com.example.mysql.model.Order;
import com.example.mysql.model.RedPacketAccount;
import com.example.mysql.repository.OrderRepository;
import com.example.mysql.repository.RedPacketAccountRepository;
import com.example.oracle.model.CapitalAccount;
import com.example.oracle.model.Customer;
import com.example.oracle.repository.CapitalAccountRepository;
import com.example.oracle.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	CapitalAccountRepository capitalAccountRepository;

	@Autowired
	RedPacketAccountRepository redPacketAccountRepository;

	@Autowired
	OrderService orderService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void testRollback() throws Exception{
		Customer tmp = customerRepository.findById(1L).orElse(null);
		tmp.setName(tmp.getName());
		tmp.setAge(Math.toIntExact(Math.round(Math.random() * 50)));
		customerRepository.save(tmp);
		Order order = new Order();
		order.setCode(112);
		order.setQuantity(11);
		try {
			orderService.saveOrder(order);
		} catch (Exception e){
			e.printStackTrace();
		}
		//throw new StoreException();
	}

	@Override
	public void testRollback2() throws Exception {
		Order order = new Order();
		order.setCode(112);
		order.setQuantity(11);
		orderService.create(order);
	}

	@Override
	@Transactional
	public void store(Customer customer, Order order) {
		customerRepository.save(customer);
		orderRepository.save(order);
	}


	@Override
	@Transactional(rollbackFor = StoreException.class)
	public void storeWithStoreException(Customer customer, Order order) throws StoreException {
		customerRepository.save(customer);
		orderRepository.save(order);
		throw new StoreException();
	}

	@Transactional(noRollbackFor = NoRollbackException.class, rollbackFor = StoreException.class)
	@Override
	public void storeWithNoRollbackException(Customer customer, Order order) throws NoRollbackException {
		customerRepository.save(customer);
		orderRepository.save(order);
		throw new NoRollbackException();
	}

	@Transactional()
	public void transfer() {
		CapitalAccount ca1  = capitalAccountRepository.findById(1l).orElse(null);
		CapitalAccount ca2 = capitalAccountRepository.findById(2l).orElse(null);
		RedPacketAccount rp1 = redPacketAccountRepository.findById(1l).orElse(null);
		RedPacketAccount rp2 = redPacketAccountRepository.findById(2l).orElse(null);
		BigDecimal capital = BigDecimal.TEN;
		BigDecimal red = BigDecimal.TEN;
		ca1.transferFrom(capital);
		ca2.transferTo(capital);
		capitalAccountRepository.save(ca1);
		capitalAccountRepository.save(ca2);
//		if (rp2.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
//			throw new RuntimeException("账号异常");
//		}
		rp2.transferFrom(red);
		rp1.transferTo(red);
		redPacketAccountRepository.save(rp1);
		redPacketAccountRepository.save(rp2);
		
	}
	
	@Transactional(rollbackFor = StoreException.class)
	public void transferWithStoreException() throws StoreException {
		CapitalAccount ca1 = capitalAccountRepository.findById(1l).orElse(null);
		CapitalAccount ca2 = capitalAccountRepository.findById(2l).orElse(null);
		RedPacketAccount rp1 = redPacketAccountRepository.findById(1l).orElse(null);
		RedPacketAccount rp2 = redPacketAccountRepository.findById(2l).orElse(null);
		
		BigDecimal capital = BigDecimal.TEN;
		BigDecimal red = BigDecimal.TEN;
		
		ca1.transferFrom(capital);
		ca2.transferTo(capital);
		capitalAccountRepository.save(ca1);
		capitalAccountRepository.save(ca2);
//		if (rp2.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
//			throw new RuntimeException("账号异常");
//		}
//		if (rp2.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
//			throw new IllegalArgumentException("账号异常");
//		}
		if (rp2.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new StoreException();
		}
		rp2.transferFrom(red);
		rp1.transferTo(red);
		redPacketAccountRepository.save(rp1);
		redPacketAccountRepository.save(rp2);
		
	}
	
	@Transactional(noRollbackFor = NoRollbackException.class, rollbackFor = StoreException.class)
	public void transferWithNoRollbackException() throws NoRollbackException {
		CapitalAccount ca1 = capitalAccountRepository.findById(1l).orElse(null);
		CapitalAccount ca2 = capitalAccountRepository.findById(2l).orElse(null);
		RedPacketAccount rp1 = redPacketAccountRepository.findById(1l).orElse(null);
		RedPacketAccount rp2 = redPacketAccountRepository.findById(2l).orElse(null);
		
		BigDecimal capital = BigDecimal.TEN;
		BigDecimal red = BigDecimal.TEN;
		
		ca1.transferFrom(capital);
		ca2.transferTo(capital);
		capitalAccountRepository.save(ca1);
		capitalAccountRepository.save(ca2);
		if (rp2.getBalanceAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new NoRollbackException();
		}
		rp2.transferFrom(red);
		rp1.transferTo(red);
		redPacketAccountRepository.save(rp1);
		redPacketAccountRepository.save(rp2);
		
	}
}
