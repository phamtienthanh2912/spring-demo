package com.example.mysql.repository;

import com.example.mysql.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepositoryCustom {

    @PersistenceContext(unitName = "mysqlPersistenceUnit")
    EntityManager entityManager;

    @Autowired
    @Qualifier("mysqlEntityManager")
    private EntityManagerFactory emf;

    @Override
    public List<Order> getListOrders() {
        Query query = entityManager.createNativeQuery("Select * from orders", Order.class);
        List<Order>  lst = query.getResultList();
        return lst;
    }

    @Override
    @Transactional
    public Order create(Order order) {
        try {
            //EntityManager entityManager = emf.createEntityManager();
            //entityManager.getTransaction().begin();
            Connection sourceConn =  entityManager.unwrap(Connection.class);
            sourceConn.setAutoCommit(false);
            String sql = "insert into orders(code, quantity) values (?, ?)";
            PreparedStatement ps = sourceConn.prepareStatement(sql);
            ps.setInt(1, order.getCode());
            ps.setInt(2, order.getQuantity());
            ps.execute();
            sourceConn.commit();
            Order order2 = new Order();
            order.setCode(112);
            order.setQuantity(11);
            //entityManager.getTransaction().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
