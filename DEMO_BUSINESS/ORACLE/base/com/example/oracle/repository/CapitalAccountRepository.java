package com.example.oracle.repository;


import com.example.oracle.model.CapitalAccount;
import org.springframework.data.repository.CrudRepository;

public interface CapitalAccountRepository extends CrudRepository<CapitalAccount, Long> {


}
