package com.example.main.controller;

import com.example.common.dto.ResponseDto;
import com.example.mysql.model.Order;
import com.example.mysql.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/all")
    @ResponseBody
    public ResponseDto<?> getAllOrder(){
        ResponseDto<Order> responseDto = new ResponseDto<Order>();
        responseDto.setSuccess(true);
        responseDto.setLstData(orderService.getAllOrders());
        return responseDto;
    }
}
