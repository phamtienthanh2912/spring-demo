package com.example.main.controller;

import com.example.main.configuration.security.AuthRequest;
import com.example.main.configuration.security.AuthResponse;
import com.example.main.configuration.security.JwtUtil;
import com.example.main.configuration.security.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
public class AuthenticationController {

    //@Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AuthenticationProvider authenticationProvider;

    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    MyUserDetailService myUserDetailService;

    @ResponseBody
    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthRequest authRequest) throws Exception {
        Authentication authentication = null;
        try {
            authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }
        final String jwt = jwtUtil.generateToken(new User(authentication.getName(), authentication.getCredentials().toString(), authentication.getAuthorities()));
        return ResponseEntity.ok(new AuthResponse(jwt));
    }
}
