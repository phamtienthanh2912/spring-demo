package com.example.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ServletComponentScan
//@EnableJpaRepositories(basePackages = "com.example.demo.repository")
@Lazy
@ComponentScan(basePackages = {"com.example.mysql.*", "com.example.oracle.*", "com.example.main.*"})
@EnableCaching
public class DemoServiceApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(DemoServiceApplication.class, args);
	}

}

