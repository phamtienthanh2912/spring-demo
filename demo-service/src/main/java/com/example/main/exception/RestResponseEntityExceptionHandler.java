package com.example.main.exception;

import com.example.common.dto.ResponseDto;
import com.example.common.exception.NoRollbackException;
import com.example.common.exception.StoreException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.ServletException;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "This should be application specific";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {NoRollbackException.class})
    protected ResponseEntity<?> handleExceptionNoRollback(final Exception e) {
        logger.error(e.getMessage(), e);
        return new ResponseEntity<>(new ResponseDto(false, e.getMessage(), "Ko roll back data"), HttpStatus.OK);
    }

    @ExceptionHandler(value = {StoreException.class})
    protected ResponseEntity<?> handleExceptionRollback(final Exception e) {
        logger.error(e.getMessage(), e);
        return new ResponseEntity<>(new ResponseDto(false, e.getMessage(), "Roll back data"), HttpStatus.OK);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<?> handleException(final Exception e) {
        logger.error(e.getMessage(), e);
        return new ResponseEntity<>(new ResponseDto(false, e.getMessage(), "Exception"), HttpStatus.OK);
    }
}
