package com.example.main.configuration;

import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

//@Configuration
public class ApplicationInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //Cau hinh trong file application.properties do uu tien cao hon
        servletContext.setInitParameter(
                "spring.profiles.active", "real");
    }
}
